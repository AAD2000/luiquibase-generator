FROM adoptopenjdk/openjdk11

COPY /build/libs/liquibase-generator-0.0.1-SNAPSHOT.jar liquibase-generator-0.0.1-SNAPSHOT.jar

CMD ["java", "-jar", "liquibase-generator-0.0.1-SNAPSHOT.jar"]