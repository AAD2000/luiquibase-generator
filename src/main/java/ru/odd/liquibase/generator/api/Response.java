package ru.odd.liquibase.generator.api;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Response {

  public static final String STATUS_SUCCESS = "success";
  public static final String STATUS_ERROR = "error";

  public String message;

  public String status;

  public Long id;
}
