package ru.odd.liquibase.generator.api.foreign;

import java.time.LocalDateTime;
import lombok.Data;

@Data
public class AddColumn {

  private Long id;

  private Boolean autoIncrement;

  private LocalDateTime createdDate;

  private String name;

  private Boolean nullable;

  private Boolean primaryKey;

  private String tableName;

  private String type;

  private Boolean unique;
}
