package ru.odd.liquibase.generator.api.foreign;

import java.time.LocalDateTime;
import lombok.Data;

@Data
public class AddSql {

  private Long id;

  private String sql;

  private LocalDateTime createdDate;

  private Boolean ddl;
}
