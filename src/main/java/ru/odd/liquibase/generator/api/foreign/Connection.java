package ru.odd.liquibase.generator.api.foreign;

import java.time.LocalDateTime;
import lombok.Data;

@Data
public class Connection {

  private Long id;

  private String baseTableName;

  private String baseColumnNames;

  private String constraintName;

  private String referencedTableName;

  private String referencedColumnNames;

  private LocalDateTime createdDate;
}
