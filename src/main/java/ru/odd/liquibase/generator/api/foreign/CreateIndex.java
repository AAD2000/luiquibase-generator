package ru.odd.liquibase.generator.api.foreign;

import java.time.LocalDateTime;
import java.util.List;
import lombok.Data;

@Data
public class CreateIndex {

  private Long id;

  private Boolean unique;

  private String name;

  private String tableName;

  private LocalDateTime createdDate;

  private List<CreateIndexColumn> createIndexColumns;
}
