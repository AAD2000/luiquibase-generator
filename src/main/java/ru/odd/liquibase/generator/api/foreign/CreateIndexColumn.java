package ru.odd.liquibase.generator.api.foreign;

import java.time.LocalDateTime;
import lombok.Data;

@Data
public class CreateIndexColumn {

  private Long id;

  private String name;

  private LocalDateTime createdDate;
}
