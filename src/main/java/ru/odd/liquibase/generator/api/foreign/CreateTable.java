package ru.odd.liquibase.generator.api.foreign;

import java.time.LocalDateTime;
import java.util.List;
import lombok.Data;

@Data
public class CreateTable {

  private Long id;

  private LocalDateTime createdDate;

  private String name;

  private List<TableColumn> tableColumns;
}
