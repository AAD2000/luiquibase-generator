package ru.odd.liquibase.generator.api.foreign;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DataBaseInfo {

  private String url;

  private String username;

  private String password;
}
