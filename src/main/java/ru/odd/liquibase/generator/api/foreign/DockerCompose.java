package ru.odd.liquibase.generator.api.foreign;

import java.time.LocalDateTime;
import java.util.List;
import lombok.Data;

@Data
public class DockerCompose {

  private Long id;

  private LocalDateTime createdDate;

  private String modelName;

  private Long openPort;

  private Boolean created;

  private String password;

  private String userName;

  private String owner;

  private String host;

  private String url;

  private String database;

  private List<Version> versions;
}
