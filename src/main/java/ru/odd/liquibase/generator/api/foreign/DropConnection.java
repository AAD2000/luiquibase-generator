package ru.odd.liquibase.generator.api.foreign;

import java.time.LocalDateTime;
import lombok.Data;

@Data
public class DropConnection {

  private Long id;

  private String name;

  private String baseTableName;

  private LocalDateTime createdDate;
}
