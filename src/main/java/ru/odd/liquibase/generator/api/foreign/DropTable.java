package ru.odd.liquibase.generator.api.foreign;

import java.time.LocalDateTime;
import lombok.Data;

@Data
public class DropTable {

  private Long id;

  private String name;

  private LocalDateTime createdDate;

  private Boolean cascade;
}
