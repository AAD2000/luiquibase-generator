package ru.odd.liquibase.generator.api.foreign;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

@Data
@JsonSerialize
public class EmptyObject {

}
