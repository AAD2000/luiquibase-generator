package ru.odd.liquibase.generator.api.foreign;

import java.util.List;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class FileContentDto {

  private List<FileData> fileDataList;

  private boolean foreign;

  private String version;

  private List<String> problems;

  private Boolean containsDdlSql;
}
