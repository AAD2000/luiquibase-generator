package ru.odd.liquibase.generator.api.foreign;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class FileData {

  private String path;

  private String content;
}
