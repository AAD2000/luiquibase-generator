package ru.odd.liquibase.generator.api.foreign;

import java.time.LocalDateTime;
import lombok.Data;

@Data
public class TableColumn {

  private Long id;

  private Boolean autoIncrement;

  private LocalDateTime createdDate;

  private String name;

  private Boolean nullable;

  private Boolean primaryKey;

  private String type;

  private Boolean unique;
}
