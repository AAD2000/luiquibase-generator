package ru.odd.liquibase.generator.api.foreign;

import java.time.LocalDateTime;
import java.util.List;
import lombok.Data;

@Data
public class Version {

  private Long id;

  private LocalDateTime createdDate;

  private boolean deployed;

  private String versionName;

  private List<DropIndex> dropIndices;

  private List<DropConnection> dropConnections;

  private List<CreateTable> createTables;

  private List<CreateIndex> createIndices;

  private List<AddSql> addSqls;

  private List<AddColumn> addColumns;

  private List<DropTable> dropTables;

  private List<DropColumn> dropColumns;

  private List<Connection> connections;
}
