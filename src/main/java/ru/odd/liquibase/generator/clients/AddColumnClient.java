package ru.odd.liquibase.generator.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import ru.odd.liquibase.generator.api.foreign.AddColumn;

@FeignClient(name = "repository-rest-resource-add-column", url = "${odd.rrs-url}")
public interface AddColumnClient {

  @PostMapping("/add-column")
  void create(@RequestParam(name = "v") Long v, @RequestBody AddColumn entity);

  @DeleteMapping("/add-column/{id}")
  void delete(@PathVariable(name = "id") Long id);

  @PutMapping("/add-column/{id}")
  void update(
      @RequestParam(name = "v") Long v,
      @PathVariable(name = "id") Long id,
      @RequestBody AddColumn addColumn);
}
