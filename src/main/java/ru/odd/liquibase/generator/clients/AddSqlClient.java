package ru.odd.liquibase.generator.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import ru.odd.liquibase.generator.api.foreign.AddSql;

@FeignClient(name = "repository-rest-resource-add-sql", url = "${odd.rrs-url}")
public interface AddSqlClient {

  @PostMapping("/add-sql")
  void create(@RequestParam(name = "v") Long v, @RequestBody AddSql entity);

  @DeleteMapping("/add-sql/{id}")
  void delete(@PathVariable(name = "id") Long id);

  @PutMapping("/add-sql/{id}")
  void update(
      @RequestParam(name = "v") Long v,
      @PathVariable(name = "id") Long id,
      @RequestBody AddSql entity);
}
