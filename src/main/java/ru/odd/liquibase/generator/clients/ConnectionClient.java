package ru.odd.liquibase.generator.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import ru.odd.liquibase.generator.api.foreign.Connection;

@FeignClient(name = "repository-rest-resource-connection", url = "${odd.rrs-url}")
public interface ConnectionClient {

  @PostMapping("/connection")
  void create(@RequestParam(name = "v") Long v, @RequestBody Connection entity);

  @DeleteMapping("/connection/{id}")
  void delete(@PathVariable(name = "id") Long id);

  @PutMapping("/connection/{id}")
  void update(
      @RequestParam(name = "v") Long v,
      @PathVariable(name = "id") Long id,
      @RequestBody Connection entity);
}
