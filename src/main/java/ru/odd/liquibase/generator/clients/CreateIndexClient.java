package ru.odd.liquibase.generator.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import ru.odd.liquibase.generator.api.foreign.CreateIndex;

@FeignClient(name = "repository-rest-resource-create-index", url = "${odd.rrs-url}")
public interface CreateIndexClient {

  @PostMapping("/create-index")
  void create(@RequestParam(name = "v") Long v, @RequestBody CreateIndex entity);

  @DeleteMapping("/create-index/{id}")
  void delete(@PathVariable(name = "id") Long id);

  @PutMapping("/create-index/{id}")
  void update(
      @RequestParam(name = "v") Long v,
      @PathVariable(name = "id") Long id,
      @RequestBody CreateIndex entity);

  @GetMapping("/create-index/{id}")
  CreateIndex get(@PathVariable(name = "id") Long id);
}
