package ru.odd.liquibase.generator.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import ru.odd.liquibase.generator.api.foreign.CreateTable;

@FeignClient(name = "repository-rest-resource-create-table", url = "${odd.rrs-url}")
public interface CreateTableClient {

  @GetMapping("/create-table")
  CreateTable getTable(
      @RequestParam(name = "owner") String owner,
      @RequestParam(name = "model") String modelName,
      @RequestParam(name = "name") String tableName);

  @PostMapping("/create-table")
  void create(@RequestParam(name = "v") Long v, @RequestBody CreateTable entity);

  @DeleteMapping("/create-table/{id}")
  void delete(@PathVariable(name = "id") Long id);

  @PutMapping("/create-table/{id}")
  void update(
      @RequestParam(name = "v") Long v,
      @PathVariable(name = "id") Long id,
      @RequestBody CreateTable entity);
}
