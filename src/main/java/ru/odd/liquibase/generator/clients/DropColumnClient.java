package ru.odd.liquibase.generator.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import ru.odd.liquibase.generator.api.foreign.DropColumn;

@FeignClient(name = "repository-rest-resource-drop-column", url = "${odd.rrs-url}")
public interface DropColumnClient {

  @PostMapping("/drop-column")
  void create(@RequestParam(name = "v") Long v, @RequestBody DropColumn entity);

  @DeleteMapping("/drop-column/{id}")
  void delete(@PathVariable(name = "id") Long id);

  @PutMapping("/drop-column/{id}")
  void update(
      @RequestParam(name = "v") Long v,
      @PathVariable(name = "id") Long id,
      @RequestBody DropColumn entity);
}
