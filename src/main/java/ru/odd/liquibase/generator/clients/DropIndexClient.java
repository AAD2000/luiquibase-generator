package ru.odd.liquibase.generator.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import ru.odd.liquibase.generator.api.foreign.DropIndex;

@FeignClient(name = "repository-rest-resource-drop-index", url = "${odd.rrs-url}")
public interface DropIndexClient {

  @PostMapping("/drop-index")
  void create(@RequestParam(name = "v") Long v, @RequestBody DropIndex entity);

  @DeleteMapping("/drop-index/{id}")
  void delete(@PathVariable(name = "id") Long id);

  @PutMapping("/drop-index/{id}")
  void update(
      @RequestParam(name = "v") Long v,
      @PathVariable(name = "id") Long id,
      @RequestBody DropIndex entity);
}
