package ru.odd.liquibase.generator.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import ru.odd.liquibase.generator.api.foreign.CreateIndexColumn;

@FeignClient(name = "repository-rest-resource-index-column", url = "${odd.rrs-url}")
public interface IndexColumnClient {

  @PostMapping("/index-column")
  Long create(
      @RequestParam(name = "v") Long v,
      @RequestParam(name = "ct") Long ct,
      @RequestBody CreateIndexColumn entity);

  @DeleteMapping("/index-column/{id}")
  void delete(@PathVariable(name = "id") Long id);

  @PutMapping("/index-column/{id}")
  void update(
      @RequestParam(name = "v") Long v,
      @RequestParam(name = "ct") Long ct,
      @PathVariable(name = "id") Long id,
      @RequestBody CreateIndexColumn entity);
}
