package ru.odd.liquibase.generator.clients;

import java.util.List;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import ru.odd.liquibase.generator.api.foreign.DataBaseInfo;
import ru.odd.liquibase.generator.api.foreign.DockerCompose;
import ru.odd.liquibase.generator.api.foreign.FileContentDto;

@FeignClient(name = "repository-rest-resource", url = "${odd.rrs-url}")
public interface RepositoryRestResourceClient {
  @PostMapping("/docker-compose")
  DockerCompose save(@RequestBody DockerCompose dockerCompose);

  @GetMapping("/docker-compose")
  List<DockerCompose> getDockerComposes(@RequestParam(name = "owner") String owner);


  @DeleteMapping("/docker-compose/{id}")
  boolean delete(@PathVariable(name = "id") Long id);

  @GetMapping("/docker-compose/url/{id}")
  DataBaseInfo dataBaseInfo(@PathVariable(name = "id") Long id);

  @GetMapping("/docker-compose/{id}")
  DockerCompose dockerCompose(@PathVariable(name = "id") Long id);

  @GetMapping("/docker-compose/ports")
  List<Long> getUsedPorts();

  @GetMapping("/docker-compose/model")
  DockerCompose getDockerComposes(@RequestParam(name = "owner") String owner, @RequestParam(name = "model") String model);

  @GetMapping("/docker-compose/{id}/files")
  FileContentDto getFileDatas(
      @PathVariable(name = "id") Long id,
      @RequestParam(name = "owner") String owner,
      @RequestParam(name = "model") String model,
      @RequestParam(name = "rollback", defaultValue = "false") boolean rollback,
      @RequestParam(name = "tag", required = false) String tag,
      @RequestParam(name = "force", defaultValue = "false") boolean force);
}
