package ru.odd.liquibase.generator.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import ru.odd.liquibase.generator.api.foreign.Version;

@FeignClient(name = "repository-rest-resource-verion", url = "${odd.rrs-url}")
public interface VersionClient {

  @PostMapping("/version")
  void create(@RequestParam(name = "dc") Long dc, @RequestBody Version version);

  @DeleteMapping("/version/{id}")
  void delete(@PathVariable(name = "id") Long id);

  @GetMapping("/version/{id}")
  Version get(@PathVariable(name = "id") Long id);
}
