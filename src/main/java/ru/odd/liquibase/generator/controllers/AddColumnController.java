package ru.odd.liquibase.generator.controllers;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.odd.liquibase.generator.api.Response;
import ru.odd.liquibase.generator.api.foreign.AddColumn;
import ru.odd.liquibase.generator.api.foreign.DockerCompose;
import ru.odd.liquibase.generator.api.foreign.Version;
import ru.odd.liquibase.generator.clients.AddColumnClient;
import ru.odd.liquibase.generator.clients.RepositoryRestResourceClient;
import ru.odd.liquibase.generator.clients.VersionClient;
import ru.odd.liquibase.generator.helpers.DockerComposeHelper;

@RestController
@AllArgsConstructor
public class AddColumnController {

  private final AddColumnClient addColumnClient;

  private final RepositoryRestResourceClient repositoryRestResourceClient;

  private final VersionClient versionClient;

  @PostMapping("/add-column")
  public Response create(
      @RequestParam(name = "v") Long v,
      @RequestBody AddColumn entity,
      @RequestParam(name = "model") String model,
      @RequestParam(name = "owner") String owner) {

    final Response response =
        DockerComposeHelper.checkAddColumn(
            repositoryRestResourceClient.getDockerComposes(owner, model),
            entity,
            versionClient.get(v).getVersionName(),
            true);

    if (response.getStatus().equals(Response.STATUS_ERROR)) {
      return response;
    }

    addColumnClient.create(v, entity);

    return response;
  }

  @DeleteMapping("/add-column/{id}")
  public void delete(@PathVariable(name = "id") Long id) {
    addColumnClient.delete(id);
  }

  @PutMapping("/add-column/{id}")
  public Response update(
      @RequestParam(name = "v") Long v,
      @PathVariable(name = "id") Long id,
      @RequestParam(name = "model") String modelName,
      @RequestBody AddColumn addColumn,
      @RequestParam(name = "owner") String owner) {

    final DockerCompose dockerComposes =
        repositoryRestResourceClient.getDockerComposes(owner, modelName);

    AddColumn addColumn1 = null;

    for (Version version : dockerComposes.getVersions()) {
      for (AddColumn column : version.getAddColumns()) {
        if (column.getId().equals(id)) {
          addColumn1 = column;
        }
      }
    }
    final Response response =
        DockerComposeHelper.checkAddColumn(
            dockerComposes,
            addColumn,
            versionClient.get(v).getVersionName(),
            !(addColumn1.getName().equals(addColumn.getName())
                && addColumn1.getTableName().equals(addColumn.getName())));

    if (response.getStatus().equals(Response.STATUS_ERROR)) {
      return response;
    }

    addColumnClient.update(v, id, addColumn);

    return response;
  }
}
