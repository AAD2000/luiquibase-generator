package ru.odd.liquibase.generator.controllers;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.odd.liquibase.generator.api.foreign.AddSql;
import ru.odd.liquibase.generator.clients.AddSqlClient;

@RestController
@AllArgsConstructor
public class AddSqlController {

  private final AddSqlClient addSqlClient;

  @PostMapping("/add-sql")
  public void create(@RequestParam(name = "v") Long v, @RequestBody AddSql entity) {
    addSqlClient.create(v, entity);
  }

  @DeleteMapping("/add-sql/{id}")
  public void delete(@PathVariable(name = "id") Long id) {
    addSqlClient.delete(id);
  }

  @PutMapping("/add-sql/{id}")
  public void update(
      @RequestParam(name = "v") Long v,
      @PathVariable(name = "id") Long id,
      @RequestBody AddSql entity) {
    addSqlClient.update(v, id, entity);
  }
}
