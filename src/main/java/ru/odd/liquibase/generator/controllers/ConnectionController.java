package ru.odd.liquibase.generator.controllers;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.odd.liquibase.generator.api.Response;
import ru.odd.liquibase.generator.api.foreign.Connection;
import ru.odd.liquibase.generator.api.foreign.DockerCompose;
import ru.odd.liquibase.generator.api.foreign.DropConnection;
import ru.odd.liquibase.generator.api.foreign.Version;
import ru.odd.liquibase.generator.clients.ConnectionClient;
import ru.odd.liquibase.generator.clients.RepositoryRestResourceClient;
import ru.odd.liquibase.generator.clients.VersionClient;
import ru.odd.liquibase.generator.helpers.DockerComposeHelper;

@RestController
@AllArgsConstructor
public class ConnectionController {

  private final ConnectionClient connectionClient;

  private final RepositoryRestResourceClient repositoryRestResourceClient;

  private final VersionClient versionClient;

  @PostMapping("/connection")
  public Response create(@RequestParam(name = "v") Long v, @RequestBody Connection entity, @RequestParam(name = "owner") String owner, @RequestParam(name = "model")String model) {
    final Response response = DockerComposeHelper
        .checkCreateConnection(repositoryRestResourceClient.getDockerComposes(owner, model),
            versionClient.get(v).getVersionName(), entity);
    if(response.getStatus().equals(Response.STATUS_ERROR)){
      return response;
    }
    connectionClient.create(v, entity);

    return response;
  }

  @DeleteMapping("/connection/{id}")
  public void delete(@PathVariable(name = "id") Long id) {
    connectionClient.delete(id);
  }

  @PutMapping("/connection/{id}")
  public Response update(
      @RequestParam(name = "v") Long v,
      @PathVariable(name = "id") Long id,
      @RequestBody Connection entity, @RequestParam(name = "owner") String owner, @RequestParam(name = "model")String model) {
    final Response response = DockerComposeHelper
        .checkCreateConnection(repositoryRestResourceClient.getDockerComposes(owner, model),
            versionClient.get(v).getVersionName(), entity);
    if(response.getStatus().equals(Response.STATUS_ERROR)){
      return response;
    }
    connectionClient.update(v,id, entity);

    return response;
  }
}
