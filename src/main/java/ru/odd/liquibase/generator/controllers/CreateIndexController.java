package ru.odd.liquibase.generator.controllers;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.odd.liquibase.generator.api.Response;
import ru.odd.liquibase.generator.api.foreign.CreateIndex;
import ru.odd.liquibase.generator.clients.CreateIndexClient;
import ru.odd.liquibase.generator.clients.RepositoryRestResourceClient;
import ru.odd.liquibase.generator.clients.VersionClient;
import ru.odd.liquibase.generator.helpers.DockerComposeHelper;

@RestController
@AllArgsConstructor
public class CreateIndexController {

  private final CreateIndexClient createIndexClient;
  private final RepositoryRestResourceClient repositoryRestResourceClient;
  private final VersionClient versionClient;

  @PostMapping("/create-index")
  public Response create(
      @RequestParam(name = "v") Long v,
      @RequestBody CreateIndex entity,
      @RequestParam(name = "owner") String owner,
      @RequestParam(name = "model") String model) {
    final Response response =
        DockerComposeHelper.checkCreateIndex(
            repositoryRestResourceClient.getDockerComposes(owner, model),
            versionClient.get(v).getVersionName(),
            entity);
    if (response.getStatus().equals(Response.STATUS_ERROR)) {
      return response;
    }
    createIndexClient.create(v, entity);

    return response;
  }

  @DeleteMapping("/create-index/{id}")
  public void delete(@PathVariable(name = "id") Long id) {
    createIndexClient.delete(id);
  }

  @GetMapping("/create-index/{id}")
  public CreateIndex get(@PathVariable(name = "id") Long id) {
    return createIndexClient.get(id);
  }

  @PutMapping("/create-index/{id}")
  public Response update(
      @RequestParam(name = "v") Long v,
      @PathVariable(name = "id") Long id,
      @RequestBody CreateIndex entity,
      @RequestParam(name = "owner") String owner,
      @RequestParam(name = "model") String model) {
    final Response response =
        DockerComposeHelper.checkCreateIndex(
            repositoryRestResourceClient.getDockerComposes(owner, model),
            versionClient.get(v).getVersionName(),
            entity);
    if (response.getStatus().equals(Response.STATUS_ERROR)) {
      return response;
    }
    createIndexClient.update(v, id, entity);

    return response;
  }
}
