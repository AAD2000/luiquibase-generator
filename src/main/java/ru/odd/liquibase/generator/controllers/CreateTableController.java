package ru.odd.liquibase.generator.controllers;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.odd.liquibase.generator.api.Response;
import ru.odd.liquibase.generator.api.foreign.CreateTable;
import ru.odd.liquibase.generator.clients.CreateTableClient;
import ru.odd.liquibase.generator.clients.RepositoryRestResourceClient;
import ru.odd.liquibase.generator.clients.VersionClient;
import ru.odd.liquibase.generator.helpers.DockerComposeHelper;

@RestController
@AllArgsConstructor
public class CreateTableController {

  private final CreateTableClient createTableClient;
  private final RepositoryRestResourceClient repositoryRestResourceClient;
  private final VersionClient versionClient;

  @PostMapping("/create-table")
  public Response create(
      @RequestParam(name = "v") Long v,
      @RequestBody CreateTable entity,
      @RequestParam(name = "owner") String owner,
      @RequestParam(name = "model") String model) {
    final Response response =
        DockerComposeHelper.checkCreateCreateTable(
            repositoryRestResourceClient.getDockerComposes(owner, model),
            entity,
            versionClient.get(v).getVersionName());

    if (response.getStatus().equals(Response.STATUS_ERROR)) {
      return response;
    }

    createTableClient.create(v, entity);

    return response;
  }

  @DeleteMapping("/create-table/{id}")
  public Response delete(
      @PathVariable(name = "id") Long id,
      @RequestParam(name = "owner") String owner,
      @RequestParam(name = "model") String model) {
    final Response response =
        DockerComposeHelper.checkDeleteCreateTable(
            repositoryRestResourceClient.getDockerComposes(owner, model), id);

    if (response.getStatus().equals(Response.STATUS_ERROR)) {
      return response;
    }
    createTableClient.delete(id);

    return response;
  }

  @PutMapping("/create-table/{id}")
  public Response update(
      @RequestParam(name = "v") Long v,
      @PathVariable(name = "id") Long id,
      @RequestBody CreateTable entity,
      @RequestParam(name = "owner") String owner,
      @RequestParam(name = "model") String model) {
    final Response response =
        DockerComposeHelper.checkEditCreateTable(
            repositoryRestResourceClient.getDockerComposes(owner, model), id, entity);

    if (response.getStatus().equals(Response.STATUS_ERROR)) {
      return response;
    }
    createTableClient.update(v, id, entity);

    return response;
  }
}
