package ru.odd.liquibase.generator.controllers;

import java.util.Collections;
import java.util.List;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.odd.liquibase.generator.api.foreign.DockerCompose;
import ru.odd.liquibase.generator.services.DockerComposeService;

@RestController
@AllArgsConstructor
@RequestMapping("/docker-compose")
public class DockerComposeController {

  private final DockerComposeService dockerComposeService;

  @PostMapping
  public DockerCompose create(@RequestParam(name = "owner") String owner, @RequestBody DockerCompose dockerCompose) {
    dockerCompose.setOwner(owner);
    return dockerComposeService.create(dockerCompose);
  }

  @GetMapping("/{id}")
  public DockerCompose dockerCompose(@PathVariable(name = "id") Long id) {
    final DockerCompose dockerCompose = dockerComposeService.dockerCompose(id);
    Collections.reverse(dockerCompose.getVersions());
    return dockerCompose;
  }

  @GetMapping
  public List<DockerCompose> dockerComposeList(@RequestParam(name = "owner") String owner){
    return dockerComposeService.dockerComposes(owner);
  }

  @DeleteMapping("/{id}")
  public void delete(@PathVariable(name = "id") Long id) {
    dockerComposeService.delete(id);
  }
}
