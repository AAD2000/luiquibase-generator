package ru.odd.liquibase.generator.controllers;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.odd.liquibase.generator.api.Response;
import ru.odd.liquibase.generator.api.foreign.DropColumn;
import ru.odd.liquibase.generator.clients.DropColumnClient;
import ru.odd.liquibase.generator.clients.RepositoryRestResourceClient;
import ru.odd.liquibase.generator.clients.VersionClient;
import ru.odd.liquibase.generator.helpers.DockerComposeHelper;

@RestController
@AllArgsConstructor
public class DropColumnController {

  private final DropColumnClient dropColumnClient;

  private final VersionClient versionClient;

  private final RepositoryRestResourceClient repositoryRestResourceClient;

  @PostMapping("/drop-column")
  public Response create(@RequestParam(name = "v") Long v, @RequestBody DropColumn entity, @RequestParam(name="owner") String owner,
      @RequestParam(name="model")String model) {

    final Response response = DockerComposeHelper
        .checkDropColumn(repositoryRestResourceClient.getDockerComposes(owner, model),
            versionClient.get(v).getVersionName(), entity);

    if(response.getStatus().equals(Response.STATUS_ERROR)){
      return response;
    }

    dropColumnClient.create(v, entity);

    return response;
  }

  @DeleteMapping("/drop-column/{id}")
  public void delete(@PathVariable(name = "id") Long id) {
    dropColumnClient.delete(id);
  }

  @PutMapping("/drop-column/{id}")
  public void update(
      @RequestParam(name = "v") Long v,
      @PathVariable(name = "id") Long id,
      @RequestBody DropColumn entity) {
    dropColumnClient.update(v, id, entity);
  }
}
