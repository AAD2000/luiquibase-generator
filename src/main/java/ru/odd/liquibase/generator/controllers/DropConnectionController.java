package ru.odd.liquibase.generator.controllers;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.odd.liquibase.generator.api.Response;
import ru.odd.liquibase.generator.api.foreign.DropConnection;
import ru.odd.liquibase.generator.clients.DropConnectionClient;
import ru.odd.liquibase.generator.clients.RepositoryRestResourceClient;
import ru.odd.liquibase.generator.clients.VersionClient;
import ru.odd.liquibase.generator.helpers.DockerComposeHelper;

@RestController
@AllArgsConstructor
public class DropConnectionController {

  private final DropConnectionClient dropConnectionClient;
  private final RepositoryRestResourceClient repositoryRestResourceClient;
  private final VersionClient versionClient;

  @PostMapping("/drop-connection")
  public Response create(@RequestParam(name = "v") Long v, @RequestBody DropConnection entity, @RequestParam(name = "owner") String owner, @RequestParam(name = "model")String model) {
    final Response response = DockerComposeHelper
        .checkDropConnection(repositoryRestResourceClient.getDockerComposes(owner, model),
            versionClient.get(v).getVersionName(), entity);
    if(response.getStatus().equals(Response.STATUS_ERROR)){
      return response;
    }
    dropConnectionClient.create(v, entity);

    return response;
  }

  @DeleteMapping("/drop-connection/{id}")
  public void delete(@PathVariable(name = "id") Long id) {
    dropConnectionClient.delete(id);
  }

  @PutMapping("/drop-connection/{id}")
  public void update(
      @RequestParam(name = "v") Long v,
      @PathVariable(name = "id") Long id,
      @RequestBody DropConnection entity) {
    dropConnectionClient.update(v, id, entity);
  }
}
