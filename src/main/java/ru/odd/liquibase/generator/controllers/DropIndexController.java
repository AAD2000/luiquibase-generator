package ru.odd.liquibase.generator.controllers;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.odd.liquibase.generator.api.foreign.DropIndex;
import ru.odd.liquibase.generator.clients.DropIndexClient;
import ru.odd.liquibase.generator.helpers.DockerComposeHelper;

@RestController
@AllArgsConstructor
public class DropIndexController {

  private final DropIndexClient dropIndexClient;

  @PostMapping("/drop-index")
  public void create(@RequestParam(name = "v") Long v, @RequestBody DropIndex entity) {
    dropIndexClient.create(v, entity);
  }

  @DeleteMapping("/drop-index/{id}")
  public void delete(@PathVariable(name = "id") Long id) {
    dropIndexClient.delete(id);
  }

  @PutMapping("/drop-index/{id}")
  public void update(
      @RequestParam(name = "v") Long v,
      @PathVariable(name = "id") Long id,
      @RequestBody DropIndex entity) {
    dropIndexClient.update(v, id, entity);
  }
}
