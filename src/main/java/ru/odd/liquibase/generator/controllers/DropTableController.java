package ru.odd.liquibase.generator.controllers;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.odd.liquibase.generator.api.Response;
import ru.odd.liquibase.generator.api.foreign.DropTable;
import ru.odd.liquibase.generator.clients.DropTableClient;
import ru.odd.liquibase.generator.clients.RepositoryRestResourceClient;
import ru.odd.liquibase.generator.clients.VersionClient;
import ru.odd.liquibase.generator.helpers.DockerComposeHelper;

@RestController
@AllArgsConstructor
public class DropTableController {

  private final DropTableClient dropTableClient;

  private final RepositoryRestResourceClient repositoryRestResourceClient;

  private final VersionClient versionClient;

  @PostMapping("/drop-table")
  public Response create(@RequestParam(name = "v") Long v, @RequestBody DropTable entity, @RequestParam(name="owner") String owner,
      @RequestParam(name="model")String model) {
    final Response response = DockerComposeHelper
        .checkCreateDropCreateTable(repositoryRestResourceClient.getDockerComposes(owner, model), entity, versionClient.get(v).getVersionName());

    if (response.getStatus().equals(Response.STATUS_ERROR)){
      return response;
    }
    dropTableClient.create(v, entity);

    return response;
  }

  @DeleteMapping("/drop-table/{id}")
  public Response delete(@PathVariable(name = "id") Long id, @RequestParam(name="owner") String owner,
      @RequestParam(name="model")String model) {
    final Response response = DockerComposeHelper
        .checkDeleteDropCreateTable(repositoryRestResourceClient.getDockerComposes(owner, model), id);

    if (response.getStatus().equals(Response.STATUS_ERROR)){
      return response;
    }
    dropTableClient.delete(id);

    return response;
  }

  @PutMapping("/drop-table/{id}")
  public void update(
      @RequestParam(name = "v") Long v,
      @PathVariable(name = "id") Long id,
      @RequestBody DropTable entity) {
    dropTableClient.update(v, id, entity);
  }
}
