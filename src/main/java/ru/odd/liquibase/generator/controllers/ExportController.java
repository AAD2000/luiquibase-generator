package ru.odd.liquibase.generator.controllers;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.SneakyThrows;
import net.lingala.zip4j.ZipFile;
import org.apache.commons.io.FileUtils;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.odd.liquibase.generator.api.foreign.DockerCompose;
import ru.odd.liquibase.generator.api.foreign.FileContentDto;
import ru.odd.liquibase.generator.api.foreign.FileData;
import ru.odd.liquibase.generator.api.foreign.Version;
import ru.odd.liquibase.generator.clients.RepositoryRestResourceClient;
import ru.odd.liquibase.generator.controllers.ExportController.Table.Column;
import ru.odd.liquibase.generator.helpers.DockerComposeHelper;

@Controller
@AllArgsConstructor
@RequestMapping("/export")
public class ExportController {

  private final RepositoryRestResourceClient repositoryRestResourceClient;

  @SneakyThrows
  @GetMapping("/adoc")
  public ResponseEntity<ByteArrayResource> asciidoc(HttpServletResponse response, @RequestParam("owner") String owner, @RequestParam("model") String model) {
    final String adoc = adoc(owner, model);
    final byte[] bytes = adoc.getBytes(StandardCharsets.UTF_8);
    ByteArrayResource byteArrayResource = new ByteArrayResource(bytes);
    return ResponseEntity.ok()
        .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename="+model+".adoc")
        .contentType(MediaType.TEXT_PLAIN)
        .contentLength(bytes.length)
        .body(byteArrayResource);

  }

  @GetMapping("/zip")
  public ResponseEntity<ByteArrayResource> zip(HttpServletResponse response, @RequestParam("owner") String owner, @RequestParam("model") String model){
    byte[] bytes = zip(owner, model);
    ByteArrayResource byteArrayResource = new ByteArrayResource(bytes);
    return ResponseEntity.ok()
        .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename="+model+"_"+owner+".zip")
        .contentType(MediaType.TEXT_PLAIN)
        .contentLength(bytes.length)
        .body(byteArrayResource);
  }


  @SneakyThrows
  public byte[] zip(String owner, String model) {
    final DockerCompose dockerComposes = repositoryRestResourceClient
        .getDockerComposes(owner, model);

    final FileContentDto fileDatas = repositoryRestResourceClient
        .getFileDatas(dockerComposes.getId(), owner, model, false, null, false);

    final List<FileData> files = fileDatas.getFileDataList();

    File tempFolder = new File(model+"_"+owner);

    tempFolder.mkdir();

    final String path = tempFolder.getPath();

    for (FileData fd : files) {

      String fullPath = fd.getPath().replace("../projects",path);
      File file = new File(fullPath);

      final File parentFile = file.getParentFile();

      if(!parentFile.exists()){
        parentFile.mkdirs();
      }

      try (FileOutputStream fileOutputStream = new FileOutputStream(file)) {
          fileOutputStream.write(fd.getContent().getBytes());
      }
    }
    ZipFile zipFile = new ZipFile(model+"_"+owner+".zip");
    zipFile.addFolder(tempFolder);
    final byte[] bytes = FileUtils.readFileToByteArray(zipFile.getFile());

    tempFolder.delete();
    zipFile.getFile().delete();
    return bytes;
  }

  public String adoc( String owner, String model) {
    final DockerCompose dockerComposes =
        repositoryRestResourceClient.getDockerComposes(owner, model);

    final List<Version> collect =
        dockerComposes.getVersions().stream()
            .sorted(DockerComposeHelper.versionComparator)
            .collect(Collectors.toList());

    Map<String, Table> tables = new HashMap<>();

    collect.forEach(
        version -> {
          version
              .getCreateTables()
              .forEach(
                  e -> {
                    final Table table = new Table();

                    e.getTableColumns()
                        .forEach(
                            c ->
                                table
                                    .getColumns()
                                    .put(
                                        c.getName(),
                                        new Column(
                                            c.getType(),
                                            c.getPrimaryKey(),
                                            c.getUnique(),
                                            c.getNullable(),
                                            c.getAutoIncrement())));

                    tables.put(e.getName(), table);
                  });

          version
              .getAddColumns()
              .forEach(
                  e -> {
                    final Table table = tables.get(e.getTableName());
                    table
                        .getColumns()
                        .put(
                            e.getName(),
                            new Column(
                                e.getType(),
                                e.getPrimaryKey(),
                                e.getUnique(),
                                e.getNullable(),
                                e.getAutoIncrement()));
                  });

          version
              .getDropColumns()
              .forEach(
                  e ->
                      tables
                          .get(e.getTableName())
                          .getColumns()
                          .remove(e.getDropColumnInfos().get(0).getName()));

          version.getDropTables().forEach(e -> tables.remove(e.getName()));
        });

    return convertToAdoc(tables);
  }

  public String convertToAdoc(Map<String, Table> stringTableMap){
    String result = "";
    for (String s : stringTableMap.keySet()) {
      result+="==== table "+s+" ====\n"
          + "|===\n"
          + "|**name**|**type**|**pk**|**increment**|**nullable**|**unique**\n";

      final Map<String, Column> columns = stringTableMap.get(s).columns;

      for (String s1 : columns.keySet()) {
        final Column column = columns.get(s1);
        result+="|"+s1+"|"+ column.getType()+"|"+column.primaryKey+"|"+column.autoIncrement+"|"+column.nullable+"|"+column.unique+"\n";
      }
      result+="|===\n";
    }
    return result;
  }

  @Data
  public static class Table {

    private Map<String, Column> columns = new HashMap<>();

    @Data
    public static class Column {

      public Column(String type, Boolean primaryKey, Boolean unique, Boolean nullable,
          Boolean autoIncrement) {

        this.type = type;
        this.primaryKey = primaryKey != null && primaryKey;
        this.unique = unique != null && unique;
        this.nullable = nullable != null && nullable;
        this.autoIncrement = autoIncrement != null && autoIncrement;
      }

      private String type;
      private boolean primaryKey;
      private boolean unique;
      private boolean nullable;
      private boolean autoIncrement;
    }

  }
}



