package ru.odd.liquibase.generator.controllers;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.odd.liquibase.generator.api.Response;
import ru.odd.liquibase.generator.api.foreign.CreateIndexColumn;
import ru.odd.liquibase.generator.clients.IndexColumnClient;
import ru.odd.liquibase.generator.clients.RepositoryRestResourceClient;
import ru.odd.liquibase.generator.clients.VersionClient;
import ru.odd.liquibase.generator.helpers.DockerComposeHelper;

@RestController
@AllArgsConstructor
public class IndexColumnController {

  private final IndexColumnClient indexColumnClient;
  private RepositoryRestResourceClient repositoryRestResourceClient;
  private VersionClient versionClient;

  @PostMapping("/index-column")
  public Long create(
      @RequestParam(name = "v") Long v,
      @RequestParam(name = "ct") Long ct,
      @RequestBody CreateIndexColumn entity){
    return indexColumnClient.create(v, ct, entity);
  }

  @DeleteMapping("/index-column/{id}")
  public void delete(@PathVariable(name = "id") Long id){
    indexColumnClient.delete(id);
  }

  @PutMapping("/index-column/{id}")
  public Response update(
      @RequestParam(name = "v") Long v,
      @RequestParam(name = "ct") Long ct,
      @PathVariable(name = "id") Long id,
      @RequestBody CreateIndexColumn entity, @RequestParam(name = "owner") String owner, @RequestParam(name = "model")String model){
    final Response response = DockerComposeHelper
        .checkIndexColumn(repositoryRestResourceClient.getDockerComposes(owner, model),
            versionClient.get(v).getVersionName(), entity);
    if(response.getStatus().equals(Response.STATUS_ERROR)){
      return response;
    }
    indexColumnClient.update(v, ct, id, entity);

    return response;
  }
}
