package ru.odd.liquibase.generator.controllers;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.odd.liquibase.generator.api.Response;
import ru.odd.liquibase.generator.api.foreign.TableColumn;
import ru.odd.liquibase.generator.clients.RepositoryRestResourceClient;
import ru.odd.liquibase.generator.clients.TableColumnClient;
import ru.odd.liquibase.generator.helpers.DockerComposeHelper;

@RestController
@AllArgsConstructor
public class TableColumnController {

  private final TableColumnClient tableColumnService;

  private final RepositoryRestResourceClient repositoryRestResourceClient;

  @PostMapping("/table-column")
  public Long create(
      @RequestParam(name = "v") Long v,
      @RequestParam(name = "ct") Long ct,
      @RequestBody TableColumn entity) {
    return tableColumnService.create(v, ct, entity);
  }

  @DeleteMapping("/table-column/{id}")
  public void delete(@PathVariable(name = "id") Long id) {
    tableColumnService.delete(id);
  }

  @PutMapping("/table-column/{id}")
  public Response update(
      @RequestParam(name = "v") Long v,
      @RequestParam(name = "ct") Long ct,
      @PathVariable(name = "id") Long id,
      @RequestBody TableColumn entity,
      @RequestParam(name="owner") String owner,
      @RequestParam(name="model")String model) {
    final Response response = DockerComposeHelper
        .checkEditTableColumn(repositoryRestResourceClient.getDockerComposes(owner, model), entity);

    if (response.getStatus().equals(Response.STATUS_ERROR)) {
      return response;
    }

    tableColumnService.update(v, ct, id, entity);

    return response;
  }
}
