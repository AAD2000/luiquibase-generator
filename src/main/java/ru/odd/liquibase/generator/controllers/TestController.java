package ru.odd.liquibase.generator.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import freemarker.cache.StringTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.apache.commons.io.output.StringBuilderWriter;
import org.springframework.mail.MailParseException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.odd.liquibase.generator.model.Message;
import ru.odd.liquibase.generator.model.MessageTemplate;
import ru.odd.liquibase.generator.repository.MessageRepository;
import ru.odd.liquibase.generator.repository.MessageTemplateRepository;

@Component
@AllArgsConstructor
public class TestController {

  private final JavaMailSender javaMailSender;

  private final MessageRepository messageRepository;

  private final MessageTemplateRepository messageTemplateRepository;

  @SneakyThrows
  @Scheduled(fixedDelay = 1000L)
  @Transactional
  public void sendEmail() {
    final List<Message> all = messageRepository.findAll();

    for (Message m : all) {
      final Long messageTemplateId = m.getMessageTemplateId();

      final MessageTemplate messageTemplate = messageTemplateRepository.findById(messageTemplateId)
          .get();

      Configuration cfg = new Configuration(Configuration.VERSION_2_3_27);

      final ObjectMapper objectMapper = new ObjectMapper();
      Map<String, Object> root = objectMapper.readValue(m.getParams(), Map.class);
      root.put("name", "Freemarker");
      StringTemplateLoader stringTemplateLoader = new StringTemplateLoader();

      Template t = new Template("templateName", new StringReader(messageTemplate.getTemplate()), cfg);

      StringBuilderWriter writer = new StringBuilderWriter();
      t.process(root,writer);


      sendEmailToUsers(m.getEmail(),messageTemplate.getSubject(), writer.toString());
      messageRepository.delete(m);
    }
  }

  public void sendEmailToUsers(String emailId,String subject, String msg){
    MimeMessage message =javaMailSender.createMimeMessage();
    try {

      MimeMessageHelper helper = new MimeMessageHelper(message, false, "UTF-8");
      String htmlMsg = msg;
      message.setContent(htmlMsg, "text/html; charset=UTF-8");
      helper.setTo(emailId);

//      helper.setFrom("oddnoreply@mail.ru");
      helper.setText(htmlMsg, true);
      helper.setSubject(subject);
      javaMailSender.send(message);
    } catch (MessagingException e) {
    }
  }
}
