package ru.odd.liquibase.generator.controllers;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.odd.liquibase.generator.api.foreign.DockerCompose;
import ru.odd.liquibase.generator.api.foreign.Version;
import ru.odd.liquibase.generator.clients.RepositoryRestResourceClient;
import ru.odd.liquibase.generator.clients.VersionClient;

@RestController
@AllArgsConstructor
public class VersionController {

  private final VersionClient versionClient;

  private final RepositoryRestResourceClient repositoryRestResourceClient;

  @PostMapping("/version")
  public void create(@RequestParam(name = "dc") Long dc, @RequestParam(name = "major") Boolean major){
    Version version = new Version();
    final DockerCompose dockerCompose = repositoryRestResourceClient.dockerCompose(dc);

    if(dockerCompose == null){
      return;
    }

    if(dockerCompose.getVersions().isEmpty()){
      version.setVersionName("1.0");
      versionClient.create(dc, version);
      return;
    }

    final Version version1 = dockerCompose.getVersions().get(dockerCompose.getVersions().size() - 1);

    final String versionName = version1.getVersionName();

    final String[] split = versionName.split("\\.");

    int majorNumber = Integer.parseInt(split[0]);
    int minorNumber = Integer.parseInt(split[1]);

    if(major) {
      version.setVersionName((majorNumber+1)+".0");
    } else {
      version.setVersionName(majorNumber+"."+(minorNumber+1));
    }

    versionClient.create(dc, version);
  }

  @DeleteMapping("/version/{id}")
  public void delete(@PathVariable( name = "id") Long id){
    versionClient.delete(id);
  }


}
