package ru.odd.liquibase.generator.helpers;

import static ru.odd.liquibase.generator.api.Response.STATUS_ERROR;
import static ru.odd.liquibase.generator.api.Response.STATUS_SUCCESS;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import ru.odd.liquibase.generator.api.Response;
import ru.odd.liquibase.generator.api.foreign.AddColumn;
import ru.odd.liquibase.generator.api.foreign.Connection;
import ru.odd.liquibase.generator.api.foreign.CreateIndex;
import ru.odd.liquibase.generator.api.foreign.CreateIndexColumn;
import ru.odd.liquibase.generator.api.foreign.CreateTable;
import ru.odd.liquibase.generator.api.foreign.DockerCompose;
import ru.odd.liquibase.generator.api.foreign.DropColumn;
import ru.odd.liquibase.generator.api.foreign.DropConnection;
import ru.odd.liquibase.generator.api.foreign.DropTable;
import ru.odd.liquibase.generator.api.foreign.TableColumn;
import ru.odd.liquibase.generator.api.foreign.Version;

public class DockerComposeHelper {

  static String regex = "^[A-Za-z0-9_]*$";

  static List<String> types =
      Arrays.asList(
          "bigint",
          "blob",
          "boolean",
          "char",
          "clob",
          "currency",
          "datetime",
          "date",
          "decimal",
          "double",
          "float",
          "int",
          "mediumint",
          "nchar",
          "nvarchar",
          "number",
          "smallint",
          "time",
          "timestamp",
          "tinyint",
          "uuid",
          "varchar");

  public static Comparator<Version> versionComparator =
      (version, t1) -> {
        final String[] split = version.getVersionName().split("\\.");
        final String[] split1 = t1.getVersionName().split("\\.");

        if (Integer.parseInt(split[0]) > Integer.parseInt(split1[0])) {
          return 1;
        }

        if (Integer.parseInt(split[0]) == Integer.parseInt(split1[0])
            && Integer.parseInt(split[1]) > Integer.parseInt(split1[1])) {
          return 1;
        }

        if (Integer.parseInt(split[0]) == Integer.parseInt(split1[0])
            && Integer.parseInt(split[1]) == Integer.parseInt(split1[1])) {
          return 0;
        }

        return -1;
      };

  public static Response checkEditTableColumn(
      DockerCompose dockerCompose, TableColumn tableColumn) {
    final boolean contains = types.contains(tableColumn.getType().toLowerCase()) || tableColumn.getType().matches("varchar\\([0-9]+\\)");

    if (!tableColumn.getName().matches(regex)) {
      return Response.builder()
          .status(STATUS_ERROR)
          .message("Name should match regex " + regex)
          .build();
    }

    if (!contains) {
      return Response.builder()
          .status(STATUS_ERROR)
          .message(
              "Invalid type: '"
                  + tableColumn.getType()
                  + "'\n Available types: \n"
                  + types.toString())
          .build();
    }

    TableColumn tableColumn1 = null;
    CreateTable createTable1 = null;
    Version version1 = null;

    for (Version version : dockerCompose.getVersions()) {
      for (CreateTable createTable : version.getCreateTables()) {
        for (TableColumn column : createTable.getTableColumns()) {
          if (column.getId().equals(tableColumn.getId())) {
            tableColumn1 = column;
            createTable1 = createTable;
            version1 = version;
          }
        }
      }
    }

    if (!tableColumn1.getName().equals(tableColumn.getName())) {
      final List<String> allColumnsInTable =
          findAllColumnsInTable(dockerCompose, version1.getVersionName(), createTable1.getName());

      if (allColumnsInTable.contains(tableColumn.getName())) {
        return Response.builder()
            .status(STATUS_ERROR)
            .message("Column with name '" + tableColumn.getName() + "' is already exist")
            .build();
      }
    }
    return Response.builder().status(STATUS_SUCCESS).build();
  }

  public static Response checkAddColumn(
      DockerCompose dockerCompose,
      AddColumn addColumn,
      String versionName,
      boolean checkexistence) {
    final boolean contains = types.contains(addColumn.getType().toLowerCase()) || addColumn.getType().matches("varchar\\([0-9]+\\)");

    if (!addColumn.getName().matches(regex)) {
      return Response.builder()
          .status(STATUS_ERROR)
          .message("Name should match regex " + regex)
          .build();
    }

    if (!contains) {
      return Response.builder()
          .status(STATUS_ERROR)
          .message(
              "Invalid type: '"
                  + addColumn.getType()
                  + "'\n Available types: \n"
                  + types.toString())
          .build();
    }

    final Response response = checkTableExist(dockerCompose, versionName, addColumn.getTableName());

    if (response.getStatus().equals(STATUS_ERROR)) {
      return response;
    }

    if (checkexistence) {
      List<String> columns =
          findAllColumnsInTable(dockerCompose, versionName, addColumn.getTableName());

      if (columns.contains(addColumn.getName())) {
        return Response.builder().status(STATUS_ERROR).message("Column is already exist").build();
      }
    }

    return Response.builder().status(STATUS_SUCCESS).build();
  }

  private static boolean isVersionLower(Version v, String versionName) {
    final String[] split = v.getVersionName().split("\\.");
    final String[] split1 = versionName.split("\\.");

    if (Integer.parseInt(split[0]) > Integer.parseInt(split1[0])) {
      return false;
    }

    if (Integer.parseInt(split[0]) == Integer.parseInt(split1[0])
        && Integer.parseInt(split[1]) > Integer.parseInt(split1[1])) {
      return false;
    }

    if (Integer.parseInt(split[0]) == Integer.parseInt(split1[0])
        && Integer.parseInt(split[1]) == Integer.parseInt(split1[1])) {
      return false;
    }

    return true;
  }

  private static boolean isVersionEqual(Version v, String versionName) {
    final String[] split = v.getVersionName().split("\\.");
    final String[] split1 = versionName.split("\\.");

    if (Integer.parseInt(split[0]) > Integer.parseInt(split1[0])) {
      return false;
    }

    if (Integer.parseInt(split[0]) == Integer.parseInt(split1[0])
        && Integer.parseInt(split[1]) > Integer.parseInt(split1[1])) {
      return false;
    }

    if (Integer.parseInt(split[0]) == Integer.parseInt(split1[0])
        && Integer.parseInt(split[1]) == Integer.parseInt(split1[1])) {
      return true;
    }

    return false;
  }

  public static Response checkCreateCreateTable(
      DockerCompose dockerCompose, CreateTable createTable, String versionName) {
    AtomicInteger tableCounter = new AtomicInteger();

    if (!createTable.getName().matches(regex)) {
      return Response.builder()
          .status(STATUS_ERROR)
          .message("Name should match regex " + regex)
          .build();
    }

    final Stream<Version> stream = dockerCompose.getVersions().stream().sorted(versionComparator);

    stream.forEach(
        version -> {
          if (isVersionLower(version, versionName)) {
            version.getDropTables().stream()
                .forEach(
                    dropTable -> {
                      if (dropTable.getName().equals(createTable.getName())) {
                        tableCounter.getAndDecrement();
                      }
                    });
          }
        });

    final Stream<Version> stream1 = dockerCompose.getVersions().stream().sorted(versionComparator);

    stream1.forEach(
        version -> {
          if (isVersionLower(version, versionName) || isVersionEqual(version, versionName)) {
            version.getCreateTables().stream()
                .forEach(
                    createTable1 -> {
                      if (createTable1.getName().equals(createTable.getName())) {
                        tableCounter.getAndIncrement();
                      }
                    });
          }
        });

    if (tableCounter.get() != 0) {
      return Response.builder()
          .status(STATUS_ERROR)
          .message("Table with name '" + createTable.getName() + "' is already exist")
          .build();
    }

    return Response.builder().status(STATUS_SUCCESS).build();
  }

  public static Response checkDeleteCreateTable(DockerCompose dockerCompose, Long id) {

    final Stream<Version> stream = dockerCompose.getVersions().stream();

    String versionName = "";

    CreateTable createTable = null;

    for (Version version : dockerCompose.getVersions()) {
      for (CreateTable createTable1 : version.getCreateTables()) {
        if (createTable1.getId().equals(id)) {
          createTable = createTable1;
          versionName = version.getVersionName();
        }
      }
    }

    for (Version version : stream.sorted(versionComparator).collect(Collectors.toList())) {
      if (!isVersionLower(version, versionName)) {

        for (Connection connection : version.getConnections()) {

          if (connection.getBaseTableName().equals(createTable.getName())
              || connection.getReferencedTableName().equals(createTable.getName())) {
            return Response.builder()
                .status(STATUS_ERROR)
                .message(
                    "First of all you should delete all CreateConnections, CreateIndices, DropTables, AddColumns with this table which are above.")
                .build();
          }
        }

        for (CreateIndex createIndex : version.getCreateIndices()) {
          if (createIndex.getTableName().equals(createTable.getName())) {
            return Response.builder()
                .status(STATUS_ERROR)
                .message(
                    "First of all you should delete all CreateConnections, CreateIndices, DropTables, AddColumns with this table which are above.")
                .build();
          }
        }
        for (DropTable dropTable : version.getDropTables()) {
          if (dropTable.getName().equals(createTable.getName())) {
            return Response.builder()
                .status(STATUS_ERROR)
                .message(
                    "First of all you should delete all CreateConnections, CreateIndices, DropTables, AddColumns with this table which are above.")
                .build();
          }
        }
        for (AddColumn addColumn : version.getAddColumns()) {
          if (addColumn.getTableName().equals(createTable.getName())) {
            return Response.builder()
                .status(STATUS_ERROR)
                .message(
                    "First of all you should delete all CreateConnections, CreateIndices, DropTables, AddColumns with this table which are above.")
                .build();
          }
        }
      }
    }
    return Response.builder().status(STATUS_SUCCESS).build();
  }

  public static Response checkEditCreateTable(
      DockerCompose dockerCompose, Long createTable, CreateTable entity) {

    if (!entity.getName().matches(regex)) {
      return Response.builder()
          .status(STATUS_ERROR)
          .message("Name should match regex " + regex)
          .build();
    }

    final Long id = createTable;
    for (Version version : dockerCompose.getVersions()) {
      for (CreateTable table : version.getCreateTables()) {
        if (table.getId().equals(id) && !table.getName().equals(entity.getName())) {
          return checkDeleteCreateTable(dockerCompose, id);
        }
      }
    }
    return Response.builder().status(STATUS_SUCCESS).build();
  }

  public static Response checkCreateDropCreateTable(
      DockerCompose dockerCompose, DropTable dropTable, String versionName) {

    if (!dropTable.getName().matches(regex)) {
      return Response.builder()
          .status(STATUS_ERROR)
          .message("Name should match regex " + regex)
          .build();
    }

    AtomicInteger tableCounter = new AtomicInteger();

    for (Version version :
        dockerCompose.getVersions().stream()
            .sorted(versionComparator)
            .collect(Collectors.toList())) {

      if (isVersionEqual(version, versionName) || isVersionLower(version, versionName)) {
        for (CreateTable createTable : version.getCreateTables()) {
          if (createTable.getName().equals(dropTable.getName())) {
            tableCounter.getAndIncrement();
          }
        }

        for (DropTable createTable : version.getDropTables()) {
          if (createTable.getName().equals(dropTable.getName())) {
            tableCounter.getAndDecrement();
          }
        }
      }
    }
    if (tableCounter.get() <= 0) {
      return Response.builder()
          .status(STATUS_ERROR)
          .message("Table with name '" + dropTable.getName() + "' is not exist or already dropped.")
          .build();
    }

    return Response.builder().status(STATUS_SUCCESS).build();
  }

  public static Response checkDeleteDropCreateTable(DockerCompose dockerCompose, Long id) {

    DropTable dropTable = null;

    String versionName = "";

    for (Version version : dockerCompose.getVersions()) {
      for (DropTable dt : version.getDropTables()) {
        if (dt.getId().equals(id)) {
          dropTable = dt;
          versionName = version.getVersionName();
        }
      }
    }

    if (!dropTable.getName().matches(regex)) {
      return Response.builder()
          .status(STATUS_ERROR)
          .message("Name should match regex " + regex)
          .build();
    }

    AtomicInteger tableCounter = new AtomicInteger();

    for (Version version :
        dockerCompose.getVersions().stream()
            .sorted(versionComparator)
            .collect(Collectors.toList())) {

      if (!isVersionLower(version, versionName) && !isVersionEqual(version, versionName)) {
        for (CreateTable createTable : version.getCreateTables()) {
          if (createTable.getName().equals(dropTable.getName())) {
            return Response.builder()
                .status(STATUS_ERROR)
                .message(
                    "First of all you should delete  CreateTable with this table which are above.")
                .build();
          }
        }
      }
    }

    return Response.builder().status(STATUS_SUCCESS).build();
  }

  private static Response checkTableExist(DockerCompose dc, String versionName, String tableName) {
    final List<Version> collect =
        dc.getVersions().stream().sorted(versionComparator).collect(Collectors.toList());

    Collections.reverse(collect);

    for (Version version : collect) {
      if (isVersionEqual(version, versionName)) {
        for (CreateTable createTable : version.getCreateTables()) {
          if (createTable.getName().equals(tableName)) {
            return Response.builder().status(STATUS_SUCCESS).build();
          }
        }
      }

      if (isVersionLower(version, versionName)) {
        for (DropTable dropTable : version.getDropTables()) {
          if (dropTable.getName().equals(tableName)) {
            return Response.builder()
                .status(STATUS_ERROR)
                .message("Table '" + tableName + "' is not exist")
                .build();
          }
        }

        for (CreateTable createTable : version.getCreateTables()) {
          if (createTable.getName().equals(tableName)) {
            return Response.builder().status(STATUS_SUCCESS).build();
          }
        }
      }
    }
    return Response.builder()
        .status(STATUS_ERROR)
        .message("Table '" + tableName + "' is not exist or is already dropped.")
        .build();
  }

  private static List<String> findAllColumnsInTable(
      DockerCompose dockerCompose, String versionName, String tableName) {
    final List<Version> collect =
        dockerCompose.getVersions().stream().sorted(versionComparator).collect(Collectors.toList());

    int startWith = -1;

    for (int i = (collect.size() - 1); i >= 0 && startWith == -1; i--) {
      final Version version = collect.get(i);

      if (isVersionLower(version, versionName) || isVersionEqual(version, versionName)) {

        for (CreateTable createTable : version.getCreateTables()) {
          if (createTable.getName().equals(tableName)) {
            startWith = i;
          }
        }
      }
    }
    List<String> columnNames = new ArrayList<>();

    boolean stop = false;

    for (int i = startWith; i < collect.size(); i++) {
      final Version version = collect.get(i);

      for (CreateTable createTable : version.getCreateTables()) {
        if (createTable.getName().equals(tableName)) {
          columnNames.addAll(
              createTable.getTableColumns().stream()
                  .map(TableColumn::getName)
                  .collect(Collectors.toList()));
        }
      }

      for (AddColumn addColumn : version.getAddColumns()) {
        columnNames.add(addColumn.getName());
      }

      List<String> columnsToRemove = new ArrayList<>();
      for (DropColumn dropColumn : version.getDropColumns()) {
        columnsToRemove.add(dropColumn.getDropColumnInfos().get(0).getName());
      }

      for (DropTable dropTable : version.getDropTables()) {
        if (dropTable.getName().equals(tableName)) stop = true;
      }

      if (stop) {
        break;
      }

      columnNames.removeAll(columnsToRemove);
    }
    return columnNames;
  }

  public static Response checkDropColumn(
      DockerCompose dockerCompose, String version, DropColumn dropColumn) {

    final Response response = checkTableExist(dockerCompose, version, dropColumn.getTableName());

    if (STATUS_ERROR.equals(response.getStatus())) {
      return response;
    }
    final boolean contains =
        findAllColumnsInTable(dockerCompose, version, dropColumn.getTableName())
            .contains(dropColumn.getDropColumnInfos().get(0));
    if (!contains) {
      return Response.builder().message("Column not found").status(STATUS_ERROR).build();
    }
    return Response.builder().status(STATUS_SUCCESS).build();
  }

  public static Response checkCreateConnection(
      DockerCompose dockerCompose, String version, Connection connection) {

    AtomicInteger integer = new AtomicInteger(0);

    for (Version dockerComposeVersion : dockerCompose.getVersions()) {
      if (isVersionEqual(dockerComposeVersion, version)
          || isVersionLower(dockerComposeVersion, version)) {
        for (Connection dockerComposeVersionConnection : dockerComposeVersion.getConnections()) {
          if (dockerComposeVersionConnection
                  .getBaseTableName()
                  .equals(connection.getBaseTableName())
              && dockerComposeVersionConnection
                  .getConstraintName()
                  .equals(connection.getConstraintName())) {
            integer.getAndIncrement();
          }
        }

        for (DropConnection dropConnection : dockerComposeVersion.getDropConnections()) {
          if (dropConnection.getBaseTableName().equals(connection.getBaseTableName())
              && dropConnection.getName().equals(connection.getConstraintName())) {
            integer.getAndDecrement();
          }
        }
      }
    }

    if (integer.get() > 0) {
      return Response.builder()
          .status(STATUS_ERROR)
          .message("Connection is already exists.")
          .build();
    }

    Response response = checkTableExist(dockerCompose, version, connection.getBaseTableName());

    if (STATUS_ERROR.equals(response.getStatus())) {
      return response;
    }

    Response response1 =
        checkTableExist(dockerCompose, version, connection.getReferencedTableName());

    if (STATUS_ERROR.equals(response1.getStatus())) {
      return response1;
    }

    final List<String> allColumnsInTable =
        findAllColumnsInTable(dockerCompose, version, connection.getBaseTableName());

    final boolean contains = allColumnsInTable.contains(connection.getBaseColumnNames());

    if (!contains) {
      return Response.builder()
          .status(STATUS_ERROR)
          .message(
              "Column '"
                  + connection.getBaseColumnNames()
                  + "' in table '"
                  + connection.getBaseTableName()
                  + "' not exist")
          .build();
    }

    final List<String> allColumnsInTable1 =
        findAllColumnsInTable(dockerCompose, version, connection.getReferencedTableName());

    final boolean contains1 = allColumnsInTable1.contains(connection.getReferencedColumnNames());

    if (!contains1) {
      return Response.builder()
          .status(STATUS_ERROR)
          .message(
              "Column '"
                  + connection.getReferencedColumnNames()
                  + "' in table '"
                  + connection.getReferencedTableName()
                  + "' not exist")
          .build();
    }

    return Response.builder().status(STATUS_SUCCESS).build();
  }

  public static Response checkDropConnection(
      DockerCompose dockerCompose, String version, DropConnection connection) {
    for (Version dockerComposeVersion : dockerCompose.getVersions()) {
      if (!isVersionLower(dockerComposeVersion, version)) {

        if (isVersionEqual(dockerComposeVersion, version)) {
          for (DropConnection dropConnection : dockerComposeVersion.getDropConnections()) {
            if (dropConnection.getBaseTableName().equals(connection.getBaseTableName())
                && dropConnection.getName().equals(connection.getName())) {
              return Response.builder()
                  .status(STATUS_ERROR)
                  .message("Drop connection is already exist")
                  .build();
            }
          }
        }
      }
    }
    return Response.builder().status(STATUS_SUCCESS).build();
  }

  public static Response checkIndexColumn(
      DockerCompose dockerCompose, String version, CreateIndexColumn createIndexColumn) {
    CreateIndex createIndex1 = null;

    for (Version dockerComposeVersion : dockerCompose.getVersions()) {
      for (CreateIndex createIndex : dockerComposeVersion.getCreateIndices()) {
        for (CreateIndexColumn indexColumn : createIndex.getCreateIndexColumns()) {
          if (indexColumn.getId().equals(createIndexColumn.getId())) {
            createIndex1 = createIndex;
          }
        }
      }
    }

    final List<String> allColumnsInTable =
        findAllColumnsInTable(dockerCompose, version, createIndex1.getTableName());

    if (!allColumnsInTable.contains(createIndexColumn.getName())) {
      return Response.builder()
          .status(STATUS_ERROR)
          .message(
              "Column '"
                  + createIndexColumn.getName()
                  + "' not exist in table '"
                  + createIndex1.getTableName()
                  + "'")
          .build();
    }

    return Response.builder().status(STATUS_SUCCESS).build();
  }

  public static Response checkCreateIndex(
      DockerCompose dockerCompose, String version, CreateIndex createIndex) {
    return checkTableExist(dockerCompose, version, createIndex.getTableName());
  }
}
