package ru.odd.liquibase.generator.helpers;

import java.util.List;
import java.util.Random;
import lombok.experimental.UtilityClass;

@UtilityClass
public class GeneratorHelper {

  private static Random random = new Random();

  public static String getString() {
    StringBuilder str = new StringBuilder();

    for (int i = 0; i < random.nextInt(5) + 15; i++) {
      str.append(randomChar());
    }

    return str.toString();
  }

  private static char randomChar() {
    if (random.nextInt(15) < 3) {
      return ("" + random.nextInt(10)).charAt(0);
    }

    int randomChar = random.nextInt('z' - 'a' + 1) + 'a';

    if (random.nextBoolean()) {
      return ("" + (char) randomChar).toUpperCase().charAt(0);
    }

    return (char) randomChar;
  }

  public static Long randomPort(Long from, Long to, List<Long> used) {
    Long l;

    do {
      l = (long) random.nextInt(to.intValue() - from.intValue());
    } while (used.contains(l));

    return l+from;
  }
}
