//package ru.odd.liquibase.generator.helpers;
//
//import java.sql.DriverManager;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.sql.Statement;
//import ru.odd.liquibase.generator.api.foreign.AddColumn;
//import ru.odd.liquibase.generator.api.foreign.Connection;
//import ru.odd.liquibase.generator.api.foreign.CreateIndex;
//import ru.odd.liquibase.generator.api.foreign.CreateTable;
//import ru.odd.liquibase.generator.api.foreign.DataBaseInfo;
//import ru.odd.liquibase.generator.api.foreign.DropColumn;
//import ru.odd.liquibase.generator.api.foreign.DropConnection;
//import ru.odd.liquibase.generator.api.foreign.DropIndex;
//import ru.odd.liquibase.generator.api.foreign.DropTable;
//
//public class JdbcHelper {
//
//  public boolean checkCreateTable(DataBaseInfo dataBaseInfo, CreateTable createTable) {
//    try (final java.sql.Connection connection =
//            DriverManager.getConnection(
//                dataBaseInfo.getUrl(), dataBaseInfo.getUsername(), dataBaseInfo.getPassword());
//        final Statement statement = connection.createStatement()) {
//
//      final ResultSet resultSet =
//          statement.executeQuery(
//              "select exists(select * from information_schema.tables t where  t.table_name="
//                  + createTable.getName()
//                  + ")");
//
//      resultSet.next();
//
//      return resultSet.getBoolean(1);
//
//    } catch (SQLException throwables) {
//      return false;
//    }
//  }
//
//  public boolean checkAddColumn(DataBaseInfo dataBaseInfo, AddColumn addColumn) {
//    try (final java.sql.Connection connection =
//            DriverManager.getConnection(
//                dataBaseInfo.getUrl(), dataBaseInfo.getUsername(), dataBaseInfo.getPassword());
//        final Statement statement = connection.createStatement()) {
//
//      final ResultSet resultSet =
//          statement.executeQuery(
//              "exist");
//
//      return true;
//    } catch (SQLException throwables) {
//      return false;
//    }
//  }
//
//  public boolean checkCreateIndex(DataBaseInfo dataBaseInfo, CreateIndex createIndex) {
//    try (final java.sql.Connection connection =
//            DriverManager.getConnection(
//                dataBaseInfo.getUrl(), dataBaseInfo.getUsername(), dataBaseInfo.getPassword());
//        final Statement statement = connection.createStatement()) {
//
//      final ResultSet resultSet =
//          statement.executeQuery(
//              "SELECT count(*) FROM "
//                  + addColumn.getTableName()
//                  + " t where t."
//                  + addColumn.getName()
//                  + " is null");
//
//      return true;
//    } catch (SQLException throwables) {
//      return false;
//    }
//  }
//
//  public boolean checkConnection(DataBaseInfo dataBaseInfo, Connection connection) {}
//
//  public boolean checkDropCreateTable(DataBaseInfo dataBaseInfo, DropTable dropTable) {}
//
//  public boolean checkDropAddColumn(DataBaseInfo dataBaseInfo, DropColumn dropAddColumn) {}
//
//  public boolean checkDropIndex(DataBaseInfo dataBaseInfo, DropIndex dropIndex) {}
//
//  public boolean checkDropConnection(DataBaseInfo dataBaseInfo, DropConnection dropConnection) {}
//}
