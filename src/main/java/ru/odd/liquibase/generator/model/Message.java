package ru.odd.liquibase.generator.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "message")
public class Message {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id")
  private Long id;

  @Column(name = "message_template_id")
  private Long messageTemplateId;

  @Column(name = "params")
  private String params;

  @Column(name = "email")
  private String email;

  @Column(name = "status")
  private String status;
}
