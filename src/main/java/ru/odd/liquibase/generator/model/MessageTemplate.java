package ru.odd.liquibase.generator.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "message_template")
public class MessageTemplate {

  @Id
  @Column(name = "id")
  private Long id;

  @Column(name = "template")
  private String template;

  @Column(name = "subject")
  private String subject;
}
