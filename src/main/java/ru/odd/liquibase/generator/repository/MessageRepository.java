package ru.odd.liquibase.generator.repository;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import ru.odd.liquibase.generator.model.Message;

public interface MessageRepository extends CrudRepository<Message, Long> {

  List<Message> findAll();
}
