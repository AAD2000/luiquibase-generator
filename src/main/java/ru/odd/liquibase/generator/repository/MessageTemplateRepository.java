package ru.odd.liquibase.generator.repository;

import org.springframework.data.repository.CrudRepository;
import ru.odd.liquibase.generator.model.MessageTemplate;

public interface MessageTemplateRepository extends CrudRepository<MessageTemplate, Long> {

}
