package ru.odd.liquibase.generator.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.HashMap;
import java.util.List;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.odd.liquibase.generator.api.foreign.DockerCompose;
import ru.odd.liquibase.generator.clients.RepositoryRestResourceClient;
import ru.odd.liquibase.generator.helpers.GeneratorHelper;
import ru.odd.liquibase.generator.model.Message;
import ru.odd.liquibase.generator.repository.MessageRepository;

@Service
public class DockerComposeService {

  private final RepositoryRestResourceClient repositoryRestResourceClient;

  private final Long openPortFrom;

  private final Long openPortTo;

  private final MessageRepository messageRepository;

  private final String host;

  @Autowired
  public DockerComposeService(
      RepositoryRestResourceClient repositoryRestResourceClient,
      @Value("${odd.open-port.from}") Long openPortFrom,
      @Value("${odd.open-port.to}") Long openPortTo,
      @Value("${odd.host}") String host,
      MessageRepository messageRepository) {
    this.repositoryRestResourceClient = repositoryRestResourceClient;
    this.openPortFrom = openPortFrom;
    this.openPortTo = openPortTo;
    this.host = host;
    this.messageRepository = messageRepository;
  }


  @SneakyThrows
  public DockerCompose create(DockerCompose dockerCompose){
    if(dockerCompose.getUrl() == null && (dockerCompose.getCreated() == null || !dockerCompose.getCreated())){
      dockerCompose.setUserName(GeneratorHelper.getString());
      dockerCompose.setPassword(GeneratorHelper.getString());
      dockerCompose.setOpenPort(getPort());

      Message message = new Message();
      message.setEmail(dockerCompose.getOwner());
      message.setStatus("CREATED");
      message.setMessageTemplateId(1L);

      HashMap<String, String> params = new HashMap<>();
      params.put("username", dockerCompose.getUserName());
      params.put("password", dockerCompose.getPassword());
      params.put("host", host);
      params.put("portg", dockerCompose.getOpenPort().toString());
      params.put("dbname", dockerCompose.getModelName());
      params.put("db", dockerCompose.getDatabase());

      final ObjectMapper objectMapper = new ObjectMapper();

      final String s = objectMapper.writeValueAsString(params);

      message.setParams(s);

      messageRepository.save(message);

    } else {
      if(dockerCompose.getDatabase().equals("postgres")){
        dockerCompose.setUrl("jdbc:postgresql://"+dockerCompose.getHost()+":"+dockerCompose.getOpenPort()+"/"+dockerCompose.getModelName());
      } else {
        dockerCompose.setUrl("jdbc:mysql://"+dockerCompose.getHost()+":"+dockerCompose.getOpenPort()+"/"+dockerCompose.getModelName());
      }
    }

    dockerCompose.setCreated(true);

    return repositoryRestResourceClient.save(dockerCompose);
  }

  private Long getPort(){
    final List<Long> usedPorts = repositoryRestResourceClient.getUsedPorts();

    if (openPortTo-openPortFrom <=usedPorts.size()){
      throw new RuntimeException("all ports are used");
    }

    return GeneratorHelper.randomPort(openPortFrom, openPortTo, usedPorts);
  }

  public DockerCompose dockerCompose(Long id){
    return repositoryRestResourceClient.dockerCompose(id);
  }

  public void delete(Long id) {
    repositoryRestResourceClient.delete(id);
  }

  public List<DockerCompose> dockerComposes(String owner) {
    return repositoryRestResourceClient.getDockerComposes(owner);
  }
}
