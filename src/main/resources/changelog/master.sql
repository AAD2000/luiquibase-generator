--liquibase formatted sql

--changeset Alexey Danilov:6204f513-d532-4364-ae06-af3fed6a03df
create table if not exists message_template
(
    id
                bigserial
        not
            null
        constraint
            message_template_pkey
            primary
                key,
    created_date
                timestamp,
    template
                varchar(2048),
    subject varchar(255)
);

--changeset Alexey Danilov:ef6e7dff-a6db-427b-b9b5-242601f6abc5
create table if not exists message
(
    id
            bigserial
        not
            null
        constraint
            message_pkey
            primary
                key,
    message_template_id bigint references message_template,
    params varchar(2048),
    email varchar(255),
    status varchar(255)
);

--changeset Alexey Danilov:a4c11ae8-2923-472a-9f17-3f6543bcb333
insert into message_template(id,created_date, template, subject)
values (1,now(), '<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <title>Новая заявка</title>
  <style>
      body {
          margin: 0;
          color: #212122;
          background-color: #1a53bc;
          font-family: sans-serif;
      }

      .email-template {
          margin: 0 auto;
          max-width: 700px;
          padding: 40px 60px;
          box-sizing: border-box;
          background-color: #1a53bc;
      }
      .separator {
          height: 1px;
          background-color: #212122;
      }

      .inbox {
          background-color: white;
          padding: 25px;
          border-radius: 25px;
      }
  </style>
</head>
<body>
<div class="email-template">
  <div class="inbox">
    <h2>
      Thanks for creating Database in our system &#128567;
    </h2>
    <h3>
      Here is your credentials:
    </h3>

    <div class="separator"></div>
    <h4>username: ${username}</h4>
    <h4>password: ${password}</h4>
    <h4>host: ${host}</h4>
    <h4>port: ${portg}</h4>
    <h4>db name: ${dbname}</h4>
    <h4>db type: ${db}</h4>

  </div>
</div>
</body>
</html>
', 'Database credentials')
