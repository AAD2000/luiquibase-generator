<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <title>Новая заявка</title>
  <style>
      body {
          margin: 0;
          color: #212122;
          background-color: #1a53bc;
          font-family: sans-serif;
      }

      .email-template {
          margin: 0 auto;
          max-width: 700px;
          padding: 40px 60px;
          box-sizing: border-box;
      }
      .separator {
          height: 1px;
          background-color: #212122;
      }

      .inbox {
          background-color: white;
          padding: 25px;
          border-radius: 25px;
      }
  </style>
</head>
<body>
<div class="email-template">
  <div class="inbox">
    <h2>
      Thanks for creating Database in our system &#128567;
    </h2>
    <h3>
      Here is your credentials:
    </h3>

    <div class="separator"></div>
    <h4>username: {username}</h4>
    <h4>password: {password}</h4>
    <h4>host: {host}</h4>
    <h4>port: {port}</h4>
    <h4>db name: {dbname}</h4>

  </div>
</div>
</body>
</html>
